import {Button, Row, Col} from 'react-bootstrap'

export default function Banner(){
	return(
		<Row>
			<Col className="p-5">
				<h1>Lokal Surf</h1>
				<p>Surfing has never been this fun, especially in the Surfing Capital of the North.</p>
				<Button variant="primary">Book now!</Button>
			</Col>
		</Row>
	)
}
