import { useState, useEffect, useContext } from 'react'
import { Container, Card, Button, Row, Col } from 'react-bootstrap'
import { useParams, useNavigate, Link } from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'

export default function ProductView(){
	
	const navigate = useNavigate()

	const {user} = useContext(UserContext)

	const {productId} = useParams()

	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0)
	const [quantity, setquantity] = useState('')

	const order = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: quantity
			})
		})
		.then(response => response.json())
		.then(result => {
			if(typeof result.message !== undefined){
				Swal.fire({
					title: "Success",
					icon: "success",
					text: result.message
				})

				navigate("/products")
			}
		}).catch(error => {
			Swal.fire({
				title: "Oopsie daisy",
				icon: "error",
				text: "Something went wrong"
			})
		})
	}

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(response => response.json())
		.then(result => {
			setName(result.name)
			setDescription(result.description)
			setPrice(result.price)
		})
	}, [productId])




	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{span: 6, offset: 3}}>
					<Card className="my-3">
						<Card.Body>
							<Card.Title>{name}</Card.Title>

							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>Php {price}</Card.Text>


							{ user.id !== null ?
								<Button variant="primary" onClick={() => order(productId)}>Book</Button>
								:
								<Link className="btn btn-warning" to="/login">Login to Book</Link>

							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}