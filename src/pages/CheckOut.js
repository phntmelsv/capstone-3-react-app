import { useContext, useEffect } from 'react'
import { Navigate } from 'react-router-dom'
import UserContext from '../UserContext'

export default function Checkout(){
	event.preventDefault()

	const [productId, setproductId] = useState('')
	const [quantity, setquantity] = useState('')

	const initiateCheckOut = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${token}`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				productId: productId,
				quantity: quantity
			})
		})
		.then(response => response.json())
		
		.then(result => {
			if(typeof result.accessToken !== 'undefined'){
				// Not only does it set the token in the localStorage, we also set the userr details to the global 'user' state/context
				localStorage.setItem('token', result.accessToken)
				retrieveUserDetails(result.accessToken)

				// For clearing out the form
				setEmail('')
				setPassword('')

				// Show an alert
				// alert("Congratulations on logging in!")
				Swal.fire({
					title: "Thank you for ordering!",
					icon: "success",
					text: "See you in the lineup."
				})
			}
		}).catch(error => {
			// Show an error alert
			Swal.fire({
				title: "Something went wrong.",
				icon: "error",
				text: "That's a bummer - try again later."
			})
		})
}




